---
layout: about
---

# QUASE NADA SOBRE

![me](assets/images/me.png "Me")


Olá!

O meu nome é Letícia e uso esse espaço pra expressar algumas opiniões, pensamentos, fatos, fotos, confissões, gostos, entre outras coisas que acredito que cabem aqui compartilhar.

Eu nasci em Osasco, SP, mas cresci e vivi até os 17 anos em São Lourenço, uma cidadezinha do sul de Minas Gerais. Depois disso, morei por uns dois anos e meio em Nova Mutum, MT e atualmente, moro na capital paulista.

Na infância, cheguei a morar por mais ou menos um ano no nordeste, alternando entre os estados de Alagoas e Sergipe.

Volta e meia me perguntam se "moudrim" é meu sobrenome, mas na verdade é só uma palavra que formei com base em alguns fonemas em inglês: my, soul e dream.

```
mouldream > mouldreem > moudrim
```
<br>
É válido ressaltar que "moudrim" não tem um significado, apesar de as palavras-base terem. Como o usuário leticiaoliveira nunca estava disponível em nenhuma plataforma, acabei usando moudrim pra tudo e hoje é praticamente um alterego.

Este é um projeto totalmente pessoal e displicente, sem cronograma fixo de publicações e sem compromisso informativo ou didático. Cada post já se encarregará, por si só, de dizer mais sobre mim.

Seja bem-vindo! 