---
layout: post
author: Moudrim
excerpt: Quero deixar registrado aqui o quanto eu admiro e gosto de homens sarados, mas calma que não é do jeito que você está pensando...
---

# HOMENS SARADOS

Quero deixar registrado aqui o quanto eu admiro e gosto de homens sarados, mas calma que não é do jeito que você está pensando...

![Imagem corpo masculino com músculos e difinição inspirado nos fisiculturistas gregos](https://i.vgy.me/4Ujuxn.jpg){:class="image is-256x256" alt="Imagem corpo masculino com músculos e difinição inspirado nos fisiculturistas gregos da antiguidade."}

É claro que um corpo bem trabalhado é muito bonito e, apesar de eu adorar ver o desenho dos músculos, é o fator **disciplina** que me atrai.

Ninguém nasce parecendo uma escultura grega, então quando eu vejo um cara com um corpo bacana eu logo penso: “esse tem foco!!!” e começo a me questionar no que estou me tornando.

E nem estou falando de físico (apesar de precisar ganhar muita massa muscular kkkkk), mas eu tenho a consciência de que a disciplina permite que qualquer pessoa seja quem ela quiser.

Especificamente nesse quesito estético, são muitos os controles que a pessoa tem que ter sobre si mesma pra alcançar seu objetivo, porque não é apenas sobre ganhar os músculos, mas sobre manter. É um estilo de vida.

Na verdade, a partir do momento em que você decide mudar ou construir algo, passa a ser assim: um processo de constante manutenção e evolução. Você tem que estar no controle.

Incrível como não existe frase mais animadora e ao mesmo tempo tão angustiante quanto *"tudo só depende de você"*.

:):

> "Nenhum homem que não domine a si mesmo é livre."
### EPITETO