---
layout: post
author: Moudrim
excerpt: 1 - É impossível colher sem plantar; 2 - O tempo sempre recompensa quem tem paciência;
---

# APENAS FATOS

1 - É impossível colher sem plantar;

2 - O tempo sempre recompensa quem tem paciência;

3 - De grão em grão, a galinha enche o papo;

4 - Quem não suporta a dor da disciplina, arca com a dor do arrependimento (esqueça a motivação);

5 - Você sempre tem o poder de escolha, mas toda escolha tem uma consequência. Não culpe os outros pelas consequências do que você mesmo escolheu (de novo, esqueça a motivação).

Isso serve pra tudo na vida: corpo, mente, finanças.

"Não sei nem se vou estar vivo amanhã."

Sejamos sinceros: muito provavelmente, você vai.

Todos os dias, sobre todos os pontos de vista, você pode melhorar.

Tá bom do jeito que tá? Beleza.

Você SEMPRE tem uma escolha.