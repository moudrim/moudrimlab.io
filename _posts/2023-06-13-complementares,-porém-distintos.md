---
layout: post
author: Moudrim
excerpt: 1. Você pode ser apaixonado por uma pessoa, mas não necessariamente amá-la; 2. Você pode amar uma pessoa, mas não necessariamente ser apaixonado por ela; 3. Tensão sexual...
---

# COMPLEMENTARES, PORÉM DISTINTOS

1. Você pode ser apaixonado por uma pessoa, mas não necessariamente amá-la;

2. Você pode amar uma pessoa, mas não necessariamente ser apaixonado por ela;

3. Tensão sexual: nem amor, nem paixão.

Sabe quando você conhece alguém e é completamente arrebatado? Você fica bobo, encantado, a pessoa parece perfeita e um milhão de situações imaginárias rolam na mente dia e noite.

Isso é paixão e, na maioria das vezes, é completamente irracional. De modo geral, a paixão é muito intensa, emocional e impulsiva. Ao contrário do amor, ela é que é, realmente, cega.

Sobre uma paixão, só existem duas possibilidades: ou ela evolui para amor ou acaba no momento em que a cegueira passar e você começar a enxergar os defeitos que não via antes.

O amor é muito mais pé no chão do que se imagina. É sinônimo de paciência, cuidado, de querer bem. Normalmente, é só com a convivência que se descobre seu verdadeiro significado. É quando, apesar dos defeitos, você ainda quer estar ali com a pessoa.

Já o sexo não requer, necessariamente, nenhum envolvimento sentimental. Claro, deve ser consentido e com alguém por quem a pessoa se sinta atraída, mas é algo essencialmente instintivo, apenas por prazer.

Tudo acima parece óbvio e, de fato, é. Mas como é normal confundirmos as coisas, às vezes o óbvio precisa ser dito.