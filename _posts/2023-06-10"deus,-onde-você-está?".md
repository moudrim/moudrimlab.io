---
layout: post
author: Moudrim
excerpt: Em algum momento da vida, todo mundo já se perguntou...
---

# "DEUS, ONDE VOCÊ ESTÁ?"

Em algum momento da vida, todo mundo já se perguntou: "de onde viemos?", "para onde vamos?" e "por que estamos aqui?".

A ciência e a religião trazem respostas diferentes para cada uma dessas perguntas, mas ainda há aqueles que creem que ambas andam lado a lado, o que pra muitos é um completo absurdo e pra outros, um consenso que mais se aproxima da verdade.

A resolução de uma única questão responderia qualquer dúvida que viéssemos a ter sobre os porquês da vida na Terra, e essa questão central nada mais é do que a **existência** ou **inexistência de Deus**.

Sendo uma pessoa de muita fé, sempre quis aprender mais sobre Ele e os porquês da nossa própria existência.

Tive bastante contato com igrejas católicas, evangélicas, fui batizada na Igreja de Jesus Cristo dos Santos dos Últimos Dias (popularmente conhecidos como *"Mórmons"*) e, acima de tudo, sempre tive muita curiosidade quanto a crenças de outras culturas.

Por muito tempo, mesmo não frequentando mais nenhuma igreja, continuei firme e forte com minha fé, pois *"o que vai contar, de fato, são minhas obras e os sentimentos do meu coração"*, não é mesmo? Mas hoje percebo que não, porque é praticamente certo que Deus não existe e estamos abandonados no universo.

Desde sempre, o ser humano busca respostas para o sentido de sua existência, *"tudo é perfeito demais pra não ter sido planejado"*. Mas no fundo, não somos muito diferentes de um cachorro, das formigas ou das águas do mar.

**A única diferença real entre nós e qualquer outra coisa presente no universo é o nosso ego, por acreditarmos ser o centro de tudo.**

Há uma estimativa de que universo existe há cerca de 13,8 BILHÕES de anos e a Terra, há 4,5 BILHÕES. Só aí já são uns bons bilhões de diferença e, com certeza, o ser humano não teve nenhum impacto em nada disso.

Ele só foi aparecer por aqui há mais ou menos 2 milhões de anos e olha que estou falando dos seres mais primitivos. O Homo sapiens mesmo só surgiu por volta de 300 mil anos atrás.

**Religiões**

Existem algumas divergências quanto ao número total de religiões existentes, mas a quantidade mínima que encontrei foi de 10 mil. Entre elas, as mais populares são o cristianismo, o islamismo, o judaísmo e o budismo.

Só no cristianismo, encontramos ramificações como a católica, ortodoxa, protestante, anglicana e pentecostal, que ainda se subdividem em outras tantas denominações que somam muito mais de 300 igrejas.

![Citação de Steven Weinberg](https://i.vgy.me/GmnUfC.jpg){:class="image is-128x128" alt="Frase de Steven Weinberg"}