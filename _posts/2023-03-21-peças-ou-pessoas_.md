---
layout: post
author: Moudrim
excerpt: No tentar simplificar, Complicou-se tudo mais...
---
# PEÇAS OUS PESSOAS?

No tentar simplificar,

Complicou-se tudo mais.

Mais do que queria,

Mais do que previa,

Mais que em menos resultou

Ou não tenha dado em nada.

Talvez faltasse um pouco mais,

Mas valeria a pena?

Que pena, que cena.

Que trama, que drama.

Ora sou protagonista,

Ora faço figuração

E, quem diria,

Na minha própria vida.

Tão ambígua,

Tão direta,

Tão incerta de certezas,

Tão escura de clarezas

E o detalhe que me afeta

Me atormenta, me liberta

Me distorce, me conserta

Conserva,

Corrói,

Dói.

Correr cansa.

Pra que pressa se o fim da linha significa fim?