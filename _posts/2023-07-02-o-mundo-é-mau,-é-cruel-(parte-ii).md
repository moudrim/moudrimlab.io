---
layout: post
author: Moudrim
excerpt: O mundo é uma grande selva e você está na cova dos leões. Não importa o quão bom, íntegro e imaculado você seja (se é que existe alguém assim)...
---

# O MUNDO É MAU, É CRUEL (PARTE II)


> "Jacaré que pangua, vira bolsa."

O mundo é uma grande selva e você está na cova dos leões. Não importa o quão bom, íntegro e imaculado você seja (se é que existe alguém assim): ninguém liga.

O tempo todo é cobra engolindo cobra e quanto mais você demorar pra aceitar esse **fato**, mais você vai sofrer. Estou falando pra você ser um imbecil e sair por aí manipulando todo mundo? É claro que não, mas é bom entender como o mundo funciona.

Existem muitas formas de poder e que o **dinheiro** é uma delas não é nenhuma novidade. Eu realmente arrisco dizer que ele compra tudo. Se você acha que não, que ele não compra o “amor verdadeiro”, por exemplo, você está confundindo a vida real com algum filme da Disney.

Se sentir ofendido, hoje em dia, também é uma forma de poder. Provavelmente a mais ridícula, mas é a mais acessível. E pior: é velada pela narrativa de justiça, já que dá poder aos “oprimidos”.

**"Quando ficar ofendido dá poder às pessoas, elas ficam ofendidas mais facilmente."** - John Stossel

Há também as **autoridades** e, infelizmente, algumas se tornam **autoritárias**. Isso está presente em qualquer cenário, seja político, religioso ou social.

As engrenagens do mundo não têm nada a ver com valores: tudo é uma questão de **quem** dá as cartas. É por isso que é tão comum vermos julgamentos iguais com resultados diferentes. Dois pesos, duas medidas.

Como já dizia o poeta romano Juvenal, *"Quis custodiet ipsos custodes?" ("Quem vigia os vigilantes?")*

É muito fácil querer mudar o mundo na base dos discursos, quando tudo se resume a um posicionamento teórico ou quando se busca uma realidade quase impossível de ser alcançada.

Mais fácil ainda é para os que fingem querer mudar alguma coisa, mas na verdade não só estão no topo, como controlam toda essa cadeia alimentar.

> "Leopardo ou zebra: me diz, cê quer ser predador ou presa?"
<br>
**RAP LORD — HAIKAISS**