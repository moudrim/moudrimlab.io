---
layout: post
author: Moudrim
excerpt: Alegria do pecado, às vezes, toma conta de mim...
---

# CARNE E OSSO - LETRAS SÃO LETRAS

*"Alegria do pecado, às vezes, toma conta de mim*

*E é tão bom não ser divina*

*Me cobrir de humanidade me fascina*

*E me aproxima do céu*

<br>

*E eu gosto de estar na Terra cada vez mais*

*Minha boca se abre e espera*

*O direito ainda que profano*

*Do mundo ser sempre mais humano*

<br>

*Perfeição demais me agita os instintos*

*Quem se diz muito perfeito*

*Na certa encontrou um jeito insosso*

*Pra não ser de carne e osso*

<br>

*Pra não ser*

*Carne e osso"*

### Zélia Duncan