---
layout: post
author: Moudrim
excerpt: Confesso que sempre quis postar sobre isso e não tive coragem (até agora) porque a quantidade de gente que conheço que fala "eu tinha pego", "eu tinha trago" é tão grande, que por um momento de ilucidez cheguei a acreditar que eu é quem estava errada com meu trazido. Mas vamos lá...
---

# TRAGO, TODES E OUTROS CRIMES

Confesso que sempre quis postar sobre isso e não tive coragem (até agora) porque a quantidade de gente que conheço que fala "eu tinha pego", "eu tinha trago" é tão grande, que por um momento de ilucidez cheguei a acreditar que eu é quem estava errada com meu trazido. Mas vamos lá...

Sinto em te informar que se você fala "tinha trago", você consegue ser pior do que quem fala "todes".

Qualquer pessoa com a sanidade mental intacta não leva isso de pronome neutro a sério, mas quando você tenta falar bonito e assassina o verbo trazer, o máximo que você vai conseguir é ser associado a um fumante burro.

Trago e pego existem? Claro que existem, mas quando empregados da forma correta.

Alguns exemplos pra você nunca mais passar vergonha:

- Se saísse mais cedo, tinha pegado o ônibus.

- Ao ser pego sem capacete, levou uma multa.

- Se tivesse pegado um casaco, não passaria frio.

- O delinquente foi pego em flagrante.

- Quando trago meu fone, ouço música nos intervalos.

- Ele tinha trazido o dinheiro, mas foi roubado quando chegou aqui.

- O bolo foi trazido por ela.

- Se tivesse chegado antes, tinham se encontrado.

- Quando chego tarde, vou direto para a cama.

Se você tiver alguma dúvida na hora de falar, exclua o verbo ter e use apenas pegar, trazer e afins flexionados na forma correta. Por exemplo: ao invés de dizer "eu tinha trago" (meu deus), substitua por "eu trouxe".

Da mesma forma, ao invés de dizer "eu tinha pego", diga apenas "eu peguei".

Agora, mais i-n-a-d-m-i-s-s-í-v-e-l que tudo isso junto, é quem leva essa terminação pra outros verbos, como o falar: "eu tinha falo"!!! Misericórdia...