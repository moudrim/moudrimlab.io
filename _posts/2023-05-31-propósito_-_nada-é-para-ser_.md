---
layout: post
author: Moudrim
excerpt: Quanto mais estudo sobre a existência humana e todos os conflitos que envolvem essa nossa vidinha banal, mais me identifico com a frase "só sei que nada sei" (aliás, Sócrates nunca disse isso).
---

# PROPÓTISO? NADA "É PARA SER"

Quanto mais estudo sobre a existência humana e todos os conflitos que envolvem essa nossa vidinha banal, mais me identifico com a frase "só sei que nada sei" (aliás, Sócrates nunca disse isso).

Eu sempre acreditei fortemente que tudo acontece como e na hora em que tem que acontecer e sempre fui muito grata por todas as coisas, inclusive essas que lá na frente a gente vê que foi bom pro nosso amadurecimento.

Mas, consumindo e analisando alguns conteúdos, refletindo bastante e até relutando muito em relação a alguns pontos, percebi que os acontecimentos terem qualquer propósito não faz o menor sentido.

Primeiro que, se tudo tivesse um propósito, nossa vida já estaria escrita e não teríamos poder de escolha.

![Imagem com frase de Jean Paul Sarte](https://i.vgy.me/LvtDJe.jpg){:alt="O homem está condenado a ser livre, condenado porque ele criou a si e ainda assim é livre. Sarte"}

Sabe aquela história de que tudo só depende de você? Pois é. Mesmo não tendo escolhido onde ou em que circunstâncias nascemos, temos a liberdade de ser e fazer o que quisermos com o nosso presente e até planejar um futuro.

Qualquer habilidade desejada pode ser aprendida. Ser fluente em um novo idioma? Tocar algum instrumento? Não foi abençoado com uma beleza divina? Olha que existem outros fatores que fazem uma pessoa ser atraente…

Tudo é moldável.

Segundo: nada acontece porque é pra ser. Foi pra ser porque já aconteceu.

Quando algo acontece, acontece porque uma sequência de pequenas escolhas levou a tal situação e isso ser bom ou ruim não muda o fato de que ter acontecido é apenas uma consequência.

Eu poderia citar inúmeros exemplos de “nossa, se eu não tivesse… aquele dia, não teria te conhecido” e isso, por si só, já corrobora o que foi dito anteriormente.

O encontro não estava destinado a acontecer. Você acha que sim porque aconteceu, mas poderia ter encontrado qualquer outra pessoa e ter essa mesma sensação.

Terceiro: ficar esperando as coisas cairem do céu não vai te levar a lugar nenhum.

Deixar tudo como está e não fazer nada pra mudar sua realidade também é uma escolha. A partir desse fato angustiante, é só tomar uma decisão.

Escrever sobre destino e acaso engloba outros assuntos polêmicos como a existência de Deus, mas essa parte eu vou deixar pra algum outro post. 