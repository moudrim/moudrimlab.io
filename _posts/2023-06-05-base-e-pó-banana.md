---
layout: post
author: Moudrim
excerpt: Todo mundo usa algum filtro nas redes sociais pra dar aquele up na aparência e seria muito bom se esses filtros pudessem ser usados na vida real.
---

# BASE E PÓ BANANA

Todo mundo usa algum filtro nas redes sociais pra dar aquele up na aparência e seria muito bom se esses filtros pudessem ser usados na vida real.

Só que truques como esse existem e as mulheres mais femininas e vaidosas do universo já descobriram isso há séculos.

Se eu voltasse no tempo, encontrasse a antiga Letícia e dissesse que um dia ela escreveria sobre maquiagem, no mínimo, rolariam muitas risadas.

Ninguém aqui está falando pra sair na rua parecendo um palhaço, mas existem detalhes sutis que fazem toda a diferença e podem ser aplicados tanto pra homem quanto pra mulher.

E isso não tem nada a ver com quebrar padrões ou qualquer outra dessas baboseiras que são pautadas hoje em dia, mas tem a ver com autoestima. Se dá pra se sentir melhor consigo mesmo, por que não?

Coisas como olheiras, manchas ou certas imperfeições na pele podem ser corrigidas e até seus pontos fortes de beleza podem ser realçados.

Não posso ter a audácia de dizer que sou a melhor pessoa pra dar esse tipo de dica, mas lá vai:

Se eu fosse definir um kit bem básico (extremamente básico) e essencial, seria composto por:

- base;

- pó banana;

- pincel e

- rímel.

A base certa vai unificar a sua pele e é importante que ela seja do tom adequado. Fica quase imperceptível e te dá um rostinho de boneca (ou boneco).

O pó, além de dar uma segurada na base, vai dar um aspecto mais natural, pra não ficar com o rosto muito melequento e o pincel vai auxiliar nesse processo.

Agora o rímel, vou até considerar que seja opcional, mas pode acreditar que faz uma boa diferença. Já vi homens que o usam pra dar um retoque na sobrancelha e até pra preencher a barba.

Pra mulher, realça os cílios e em casos mais extremos como o meu, que tenho olhos pequenos, simplesmente parece que você tem olhos.

A parte mais chata, sinceramente, nem é aderir à maquiagem, mas as pessoas que fazem parte do seu círculo de convívio, que inevitavelmente vão fazer certo alarde com essa mudança.

Mas uma coisa eu garanto: é um caminho sem volta.