---
layout: post
author: Moudrim
excerpt: Às vezes, me surpreendo com algumas de minhas idiotice... De fato, quem milita se limita em vários aspectos, principalmente por não estar aberto ao debate...
---

# QUEM MILITA...

![Imagem com alteração de status em conta de rede social](https://i.vgy.me/hWTVc7.jpg){:class="image is-256x256" alt="Frase na imagem é: Voce alterou sua bio para quem milita se limita mata a vaca e faz marmita..."}

Às vezes, me surpreendo com algumas de minhas idiotice...
De fato, quem milita se limita em vários aspectos, principalmente por não estar aberto ao debate.

Alguns militantes (de todos os lados), apesar de afirmarem que se permitem discutir e que respeitam a opinião alheia, na verdade se comportam de maneira totalmente oposta.

Até o real significado de debate parece ter se perdido ou causado confusão semântica. O intuito não é (ou não deveria ser) atacar o outro lado, muito menos "lacrar".

Um debate é feito para apresentar argumentos, analisar os do outro e muitas vezes não leva a nenhuma resolução. É feito para ponderar.

Normalmente, um debate decente deveria levantar muito mais questões do que antes de seu início. E nem estou falando de política, mas sim de debates de um modo geral.

Um dos maiores erros do ser humano é acreditar que é um ser pronto, pois estamos em constante mudança e a transformação é algo inevitável.

Quando alguém está muito convicto de sua opinião, não se dá a oportunidade de aprender e ampliar o seu próprio eu.

![Imagem com frase de Walt Whitman](https://i.vgy.me/iKIdCV.jpg){:class="image is-256x256" alt="Frase na imagem diz: Estou a me contradizer? Pois bem então, estou a me contradizer. Eu sou amplo, eu contenho multidões."}