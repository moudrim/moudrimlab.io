---
layout: post
author: Moudrim
excerpt: Desde que o mundo é mundo, muita coisa influencia a forma como você se destaca na multidão e uma coisa é certa... ser bom não é o bastante...
---

# O MUNDO É MAU, É CRUEL

Desde que o mundo é mundo, muita coisa influencia a forma como você se destaca na multidão e uma coisa é certa: ser bom não é o bastante.

Quantos vencedores do The Voice você conhece que fazem tanto sucesso quanto MC’s de auto-tune? (Se bem que, até essas competições são... deixa pra lá.)

Não que seja impossível você alcançar algum sucesso de forma orgânica, mas é bem mais difícil quando você luta contra gente que está pagando para aparecer (entre otras cositas más).

Aliás, a própria definição de sucesso é relativa, já que todos temos ambições diferentes. Mas em âmbitos de popularidade, não é segredo pra ninguém que alguns "artistas", literalmente, pagam pela fama.

Seja para sair em matérias, entrar nas playlists do Spotify, aparecer em certos programas ou estar nos outdoors: tudo tem um dindim por trás.

Antes, de modo geral, a arte era melhor?

Na verdade, nem melhor, nem pior.

As pérolas estão apenas sufocadas em meio a tanta informação. E tudo dito até aqui está muito longe de ser uma crítica...

> *"Quando a luz acende eles me prende'*
<br>
*Eles me querem quieto, eles me querem mudo (me querem mudo)*
<br>
*E quando eu penso em tudo eu vejo o mundo*
<br>
*Eu quero em nota azuis-piscinas, tipo só por um segundo*
<br>
*Acelere, vai, sai daqui, vê se não deixa rastro*
<br>
*O mundo é mau, é cruel e vai te deixar em pedaço*
<br>
*E pra quem quer o que eu tenho, mas não faz o que eu faço*
<br>
*Cuidado pra não se afogar no lago raso*
<br>
*Soluciona esse problema, pega e vê se vale a pena*
<br>
*Nós é criado lá embaixo, Guaicurus e Afonso Pena*
<br>
*Fica a dica, fica por dentro do esquema*
<br>
*Com a conta igual quem ganhou na Mega Sena, tipo foda-se o sistema*
<br>
*Uma mão segurando o volante e com a outra dispenso o flagrante*
<br>
*Sei que a vida passa num instante, de lembrança só foto na estante*
<br>
*E a maldade nós vê no semblante da humanidade tão ignorante*
<br>
*A juventude não é mais uma banda numa propaganda de refrigerante, não*
<br>
*O mundo é mau, é cruel*
<br>
*Coca na endola, Coca-Cola retornável*
<br>
*Desenrola que o mundo é mau*
<br>
*É crue-e-e-el"*
<br>
***O MUNDO É MAU - FROID FT. CLARA LIMA***