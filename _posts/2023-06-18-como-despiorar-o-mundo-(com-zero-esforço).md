---
layout: post
author: Moudrim
excerpt: Um vídeo de mesmo título, do canal de filosofia Epifania Experiência, lista pequenas coisas que podemos fazer para "despiorar" o mundo e eu achei essa ideia muito boa. São pequenas atitudes que trarão resultados realmente efetivos, ao contrário de ficar posando de moralista no Twitter ou colocando a culpa de tudo na "sociedade machista patriarcal". Resolvi fazer minha própria lista, mas com apenas 3 itens
---

# COMO DESPIORAR O MUNDO (COM ZERO ESFORÇO)


Um vídeo de mesmo título, do canal de filosofia Epifania Experiência, lista pequenas coisas que podemos fazer para "despiorar" o mundo e eu achei essa ideia muito boa.

São pequenas atitudes que trarão resultados realmente efetivos, ao contrário de ficar posando de moralista no Twitter ou colocando a culpa de tudo na "sociedade machista patriarcal".

Resolvi fazer minha própria lista, mas com apenas 3 itens (menos esforço) e o primeiro, consta no próprio vídeo:

**1. Seja um doador de órgãos**

*"Nesse caso, você literalmente não precisa fazer nada, você vai estar morto. O cara vai lá, te abre e vê o que presta aí dentro. Top.*

*Tá, não, na verdade precisa fazer uma coisa: avisar a sua família que você quer ser doador de órgãos, porque são eles que dão a palavra final. Mas é só isso.*

*(...) Sem contar que isso resolve aquele medo que muita gente tem de ser enterrado vivo. Se te abrirem ao meio pra arrancar o seu coração na faca, certeza de que vivo você não vai ser enterrado."*

**2. Cuide da própria vida**

Não tem como dizer isso sem parecer uma indireta, mas juro que não é (pelo menos, não agora).

Se você pensar bem, cuidar da própria vida + a vida do outro, é um baita esforço. Então, se cada um cuidar da sua, todo mundo ganha.

Caso você queira muito fazer algum esforço, recomendo dar um up em si mesmo, como fazer academia, se dedicar a algum esporte, aprender inglês, abrir um negócio e etc.

Agora, se quiser fazer muito esforço e ainda cuidar da vida de outras pessoas, faça trabalho voluntário.

**3. Não faça faculdade**

Se você não tem o sonho de fazer uma faculdade, não faça e poupe o mundo de mais um profissional mal capacitado.

Eu sempre tenho um pé atrás com estatísticas, mas recentemente vi uma notícia sobre os resultados de um teste, onde quase 70% dos médicos recém-formados no Brasil não sabem identificar um infarto ou aferir pressão arterial.

Fora as inúmeras mortes por diagnóstico errado, então faz um favor pra todo mundo: não quer fazer faculdade? Não faz.