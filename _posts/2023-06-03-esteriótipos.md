---
layout: post
author: Moudrim
excerpt: Certa vez, quando ainda morava em São Lourenço, Minas Gerais, conheci uma pessoa interessante no centro da cidade, mais especificamente lá no "Calçadão".
---

# ESTERIÓTIPOS

Certa vez, quando ainda morava em São Lourenço, Minas Gerais, conheci uma pessoa interessante no centro da cidade, mais especificamente lá no "Calçadão".

Eu não lembro o nome dela, nem como ou com quem eu tinha ido parar ali (nunca fui muito de sair), mas lembro que ela se vestia totalmente de preto, usava umas correntes, botas, tinha piercing e tudo mais, enfim, visualmente era muito "trevosa" e devia ter uns vinte e poucos anos.

Cheguei a perguntar se ela era rockeira, o que seria uma pergunta idiota se ela não tivesse dito que não. Ela falou que ouvia todo tipo de música, mas que o gênero preferido era funk e naquela hora minha mente bugou.

\- Eu só gosto do estilo (risos).

Nunca mais a vi e, com certeza, ela nem se lembra disso, mas esse breve episódio me marcou bastante porque foi o meu primeiro contato com a quebra de expectativa em relação a um esteriótipo.

Hoje em dia, por uma ironia da vida, eu passo por isso constantemente. Quase todo mundo que eu acabo de conhecer me pergunta: "você é rockeira, né?".

Não que eu não ouça rock; eu já tive sim minha fase de ouvir somente isso e até ter um certo preconceito com relação a outros estilos musicais.

Mas no decorrer do tempo e passando a socializar com mais frequência (mesmo com cara de cu), além de viver morando cada hora numa cidade, aprendi a gostar de músicas de todo tipo.

Sertanejo, r&b, rock, pop, funk, indie, música clássica, samba, bluegrass, mpb, rap nerd e afins: tudo tem seu momento, de acordo com o meu estado de espírito.

Só com o reggae eu fui mais resistente, principalmente por estar muito atrelado à maconha, mas depois de conhecer a Renata, isso também já mudou.