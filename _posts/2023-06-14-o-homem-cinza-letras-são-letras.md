---
layout: post
author: Moudrim
excerpt: No ano passado (2022), o rapper Froid lançou um álbum entitulado Gado, com essa capa magnífica aqui...
---

# O HOMEM CINZA - LETRAS SÃO LETRAS

No ano passado (2022), o rapper Froid lançou um álbum entitulado Gado, com essa capa magnífica aqui:

![Capa do album músical do cantor de rap Froid](https://i.vgy.me/OdZ3fH.jpg){:class="img-responsive" alt="Capa do album músical do cantor de rap Froid. A capa possui a face de um bovido com a coroa que simboliza santos e anjos."}

Eu curto muito o estilo de escrita desse cara, o jogo de palavras e as inúmeras referências que ele sempre insere em todas as suas obras. Roubei da faixa O homem cinza o verso "Palavras são palavras, mas letras são letras" para nomear esse tópico, onde volta e meia trarei alguma letra de música que eu acho genial.

> "Me jogaram na matilha, eu voltei Villa-Lobos". - Froid