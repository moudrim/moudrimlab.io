---
layout: post
author: Moudrim
excerpt: Sabe aquelas coisas, aquelas que a gente não diz...
---

# MAIS UMA NOITE

Sabe aquelas coisas,

aquelas que a gente não diz

por não saber se o que querem é dizer

só nos fazem sentir

sem entender o porquê

talvez aqui se resumam

ou não traduzam metade

ai, que saudade de você.

(...)