---
layout: post
author: Moudrim
excerpt: Eu me lembro de quando olhava pra você...
---

# EU ME LEMBRO DE QUANDO OLHAVA PRA VOCÊ - LETRAS SÃO LETRAS


*"Eu me lembro de quando olhava pra você*

*E sabia que aquilo era tudo o que eu queria ter*

*E dormia com o resto de lua, vendo o sol nascer*

*Acordava, fazia o café e vinha te trazer*

*A rotina era um beijo no ouvido e te ver acordar*

*E acordado mesmo, eu sonhava vendo você rir*

*Viajava no jeito que você me deixava guiar*

*Cada dança que a gente ensaiava ao nascer do luar*

<br>

*Já cansada, tu vinha pedir pra fazer cafuné*

*E ligava a TV pra escolher o que a gente ia assistir*

*Quase sempre, era um filme de ação ou uma série qualquer*

*Daquelas que nunca cansava de ver repetir*

<br>

*Eu ainda não sei bem dizer o que aconteceu*

*Em qual momento tudo ia bem e a gente se perdeu*

*Às vezes, me dói não saber onde você está*

*Mas o tempo vai passando e a gente aprende a lidar*

<br>

*Foi tão frio ter que olhar pro colchão e não ver mais ninguém*

*Só o vazio preenchendo o meu peito, fazendo refém*

*E todo mundo fazendo questão de lembrar*

*Que seu corpo era o mundo que eu mais amava explorar*

*Até a toalha, aqui no banheiro, esqueceu de levar*

*Como a saudade no meu peito, as nossas fotos no espelho*

*Aquelas lembranças, não esqueci de apagar*

*Mas deixa aí que vez em quando até que me faz bem lembrar*

<br>

*Eu ainda não sei bem dizer o que aconteceu*

*Em qual momento tudo ia bem e a gente se perdeu*

*Às vezes, me dói não saber onde você está*

*Mas o tempo vai passando e a gente aprende a lidar*

<br>

*Ainda não sei bem dizer o que aconteceu*

*Em qual momento tudo ia bem e a gente se perdeu*

*Às vezes, me dói não saber como você está*

*Mas o tempo vai passando e a gente aprende a lidar*

<br>

*Ou não."*

### Noel

> P.S.: essa letra é do meu amigo de fé, meu irmão, camarada, Noel Ricardo (@noelricardoo) e eu considero uma obra-prima dentre as muitas maravilhas que ele compõe. Te amo, Ric.