---
layout: post
author: Moudrim
excerpt: Sei que é clichê, mas eu amo um bom e velho romance policial.
---

# AGATHA CHRISTIE - AUTOBIOGRAFIA

Sei que é clichê, mas eu amo um bom e velho romance policial.

Descobri esse universo literário ainda na época da escola, através de algum livro do Conan Doyle (Um estudo em vermelho, O cão dos Baskervilles ou O signo dos quatro - não lembro qual li primeiro).

Já de cara, fiquei fascinada por Sherlock Holmes e nesse período eu consumia uma quantidade considerável de livros; um atrás do outro e às vezes até dois ao mesmo tempo.

Até que, certo dia, o carinha da biblioteca me sugeriu ler um da Agatha Christie e lembro que o primeiro foi Convite para um homicídio, pois fui instantaneamente atraída pelo título.

Depois desse, li vários outros e acabei me "apaixonando"”" também por Hercule Poirot e sua criadora. Aliás, antes de saber que se pronunciava “Pôarrô”, eu falava "Pôiro", puxando um r bem caipira (kkkk).

![Imagem do mustache, o tal bigodão inspirado em Salvador Dali e novos americanos pré guerra de independência](https://i.vgy.me/lUS7ch.png){:class="image is-128x128" alt="Imagem do mustache, o tal bigodão inspirado em Salvador Dali e novos americanos pré guerra de independência."}

Li mais livros da Agatha que do Doyle e, se tem um que eu considero o melhor de todos, é a autobiografia da Rainha do crime.

Esse livro é repleto de pérolas, lições e muito bom humor. Agatha Christie, mais do que uma excelente romancista, tinha uma personalidade que passei a admirar imensamente.

Enquanto eu lia essa maravilha, vários trechos me cativaram de forma especial, mas não sei porque apenas um eu transcrevi para o caderno, o qual apresento abaixo:

"*Nas conversas com a babá, as referências à aristocracia eram frequentes. Tais referências despertavam minha ambição. Mais do que tudo no mundo, eu desejava um dia vir a ser lady Agatha. Mas os conhecimentos sociais da babá eram inexoráveis.*


> \- Isso você nunca poderá ser - disse ela.

> \- Nunca? - perguntei aterrada.

> \- Nunca — disse a babá, realista inconteste. - Para ser lady Agatha, é preciso ter nascido lady Agatha. Você tem de ser filha de um conde. Se se casar com um duque, será uma duquesa, mas isso devido ao título de seu marido. Não é algo com que tenha nascido.


*Esse foi meu primeiro contato com o inevitável. Existem coisas que não podem ser alcançadas. É importante e muito útil dar-se conta disso logo cedo na vida. Há coisas que simplesmente não se pode ter — um cacho natural no cabelo, olhos negros (quando os seus são azuis) ou o título de lady Agatha."*

![Imagem da escritora Agatha Christie quando criança](https://i.vgy.me/iYMgt2.jpg){:class="image is-128x128" alt="Imagem da escritora Agatha Christie quando criança."}

*Agatha Christie na infância*
