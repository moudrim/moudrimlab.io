---
layout: post
author: Moudrim
excerpt: Provalmente, esse será o texto mais pessoal desse site. Já de início, deixo claro que se trata apenas de ponto se vista e, em nenhum momento, da tentativa de denegrir, prejudicar ou ofender alguém. É apenas de uma forma de expressão, sem a intenção de apontar o que é certo e errado e livre de qualquer militância.
---

# LGBTQWXYZ


*Provalmente, esse será o texto mais pessoal desse site. Já de início, deixo claro que se trata apenas de ponto se vista e, em nenhum momento, da tentativa de denegrir, prejudicar ou ofender alguém. É apenas de uma forma de expressão, sem a intenção de apontar o que é certo e errado e livre de qualquer militância.*

Quando eu era criança, eu queria muito ser um menino. Eu orava todas as noites pedindo a Deus pra que, ao acordar no dia seguinte, estivesse em um outro corpo.

É muito delicado falar sobre esse tipo de coisa, principalmente porque pautas como essa estão chegando num ponto um pouco fora da curva, atualmente.

É claro que uma criança não sabe de nada da vida, mas se eu disser que esse tipo de sentimento de insatisfação não ocorre desde cedo, estarei mentindo.

Eu gostava de brincadeiras de menino, roupas de menino, só tinha amigo menino, nunca fui vaidosa e sempre me senti atraída por meninas.

Sempre desde quando? Sempre desde sempre.

Me lembro que cheguei a pedir pra treinar futebol com os garotos no campinho do bairro, mas o técnico falou que não, porque era "só pra meninos"”".

Uma outra vez, quando criaram um clube dos meninos, não me deixaram participar.

Sem contar que minhas paixonites sempre eram muito femininas e, obviamante, jamais olhariam para uma alguém como eu.

Esse tipo de coisa fazia eu me sentir mal comigo mesma e, desde que me entendo por gente, sempre tive problemas de autoestima.

As coisas começaram a mudar à medida em que fui crescendo e amadurecendo. Aos poucos, fui entendendo que eu não precisava, necessariamente, ser um menino para fazer as coisas que gostava.

A adolescência, com certeza, foi um período libertador: eu já tinha uma melhor noção de identidade, já tinha amizades femininas e sentia como se eu pudesse fazer qualquer coisa.

Como a realidade é bem diferente de filmes do tipo "De repente, 30"”", aprendi a me conformar com o que, de fato, eu era: lésbica.

"Tá, mas onde você quer chegar com tudo isso?"

Na época em que eu passei por essas coisas, esses assuntos não eram abordados da mesma forma e na proporção dos dias de hoje.

A luta pelo respeito, igualdade e liberdade virou algo maçante, principalmente por ter se tornado uma pauta usurpada pela **politicagem**.

Se quando eu era criança eu pudesse, de fato, escolher trocar de sexo, sem dúvidas, teria feito isso. Mas a Letícia de hoje teria se arrependido dessa decisão.

Resolvi escrever sobre isso porque hoje tudo está *muito hard*. **Não acho legal** que crianças possam tomar decisões drásticas e passíveis de arrependimento, mesmo que seja por vontade própria.

É claro que qualquer escolha diferente que eu fizesse no passado não me levaria a ser quem sou hoje, então não tem como saber o que eu realmente pensaria a respeito.

Se seu filho ou filha, por exemplo, apresentar comportamentos conflitantes com o gênero biológico, sugiro apoiar na forma de se vestir livremente, apoiar nas brincadeiras e atividades que ele ou ela gosta.

**Estimular a autoconfiança**, ensinar sobre **respeito**, independente de qualquer fator, seja sexual, social, econômico, racial e etc.

Quando for adulto, que decida o que fará da vida. Mas enquanto criança, **não concordo** com incentivar mudanças muito radicais.

Essa experiência por si só já é muito conflitante e delicada e, infelizmente muita gente maldosa se aproveita disso.

Fora a questão de banheiros unissex (o que sou extremante contra), existem mães impondo suas vontades nas crianças como se fosse uma escolha delas, além de muitos outros absurdos.

É cada coisa bizarra que leio nas notícias que me dá um pouco de medo ao pensar aonde tudo isso vai levar.

As lutas pela causa LGBT saíram totalmente do eixo, porque ao invés de buscar por igualdade, acabou segregando essa parcela da população em uma comunidade não apenas isolada, mas isolada até de outros LGBT's!!!

Mas meu objetivo aqui, como já mencionei, não é apontar erros. Apenas quero expressar como as coisas estão indo um pouco longe demais.

Se todo mundo for uma pessoa melhor de forma individual, consequentemente a sociedade será beneficiada. Porém, é inevitável que existam pessoas ruins.

Chega a ser um pouco inocente acreditar em uma sociedade perfeita, já que você não pode controlar todo mundo.

> "A menor minoria na Terra é o indivíduo." - Ayn Rand