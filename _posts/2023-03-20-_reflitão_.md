---
layout: post
author: Moudrim
excerpt: Às vezes eu me pergunto em que momento o ser humano deixou de exercitar a principal característica que o diferencia dos outros animais...
---

# "REFLITÃO"

Às vezes eu me pergunto em que momento o ser humano deixou de exercitar a principal característica que o diferencia dos outros animais: a razão. Ou talvez, de modo geral, ele continue essencialmente o mesmo e não sejamos assim tão racionais como acreditamos.

A forma como cada um enxerga o mundo é única, mas influenciada por inúmeros fatores e somos nós mesmos quem filtramos as informações até resultar nessa visão pessoal.

Algumas questões são tão óbvias e seriam de tão fácil resolução se realmente fossem baseadas na lógica e no que significa, de fato, ser humano, mas a verdade é que nos orientamos por nossas emoções e crenças, sendo inevitavelmente arrogantes e egoístas (alguns um pouco ou muito mais).

Não que haja um dono da razão, mas todos nós temos a falsa certeza de que a forma como pensamos é a correta. E quem pensa diferente também tem essa convicção, então quem está errado?

Não deveria ser uma questão de estar certo ou errado. Deveríamos por natureza desejar ouvir o que o outro tem a dizer, debater, ponderar, avaliar se faz algum sentido… Deveríamos ser instigados pela fome de questionar.

Mas como “estar certo” já nos basta, toda a discussão, de todas as formas e sobre todas as coisas pode ser levada ao extremo, alegando ser em benefício da própria humanidade.

Às vezes eu me pergunto por que eu me pergunto essas coisas. Quando falo de ser humano, não posso ser exceção.

A teoria é diferente da prática, afinal, a melhor opção na maioria das vezes é um “ah, vai si fudê”.