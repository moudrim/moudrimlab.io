---
layout: post
author: Moudrim
excerpt: Conhecido por sua sinceridade ímpar, Regis Tadeu não tem papas na língua quando o assunto é o cenário musical atual. Seu canal é um dos poucos que ainda acompanho no Youtube e, mesmo quando ele critica alguém que admiro muito, me acabo com sua criatividade para comparações e analogias, principalmente por serem bem específicas...
---

# REGIS TADEU

![Regis Tadeu](https://i.vgy.me/ncXDyR.jpg){:class="image is-128x128" alt="Imagem do crítico musical Regis Tadeu."}

Conhecido por sua sinceridade ímpar, Regis Tadeu não tem papas na língua quando o assunto é o cenário musical atual.

Seu canal é um dos poucos que ainda acompanho no Youtube e, mesmo quando ele critica alguém que admiro muito, me acabo com sua criatividade para comparações e analogias, principalmente por serem **bem** específicas.

Desconheço alguém com maior arsenal de figuras de linguagem.

*"(...) Eu, particularmente, tive a mesma sensação de ouvir um pescador esquimó dentro de um buraco no gelo tentando cantar como se fosse o Ice Cube no meio de uma tempestade de neve." - sobre MC Daniel*

*"(...) Uma cantora completamente soporífera, com interpretações capazes de fazer com que uma manada inteira de gnus adormeça no meio da migração pelas savanas africanas." - sobre Lana Del Rey*

*"(...) Um negócio tão engraçado quanto você assistir a um massacre de golfinhos." - sobre a origem do apelido de Jojo Todynho*

*"(...) Desafinada como uma cabeça decaptada de um corvo. A voz dela sempre entupida... sempre soando robótica, tamanha é a quantidade de auto-tune e melodyne que é colocado em estúdio na voz dela pra disfarçar a desafinação. A voz dela, cara... é como se fosse uma bolha de sabão... de sabão feito de esterco, cara, e essa bolha é impossível de ser conservada, por mais que ela seja soprada por uma laringe que parece que é feita de telha quebrada." - sobre Melody*

*"(...) Cara, a letra dessa música poderia ter sido escrita por uma lontra que tivesse dormido em cima de um livro escrito pelo Fábio Jr." — sobre "Duas metades", de Jorge e Mateus*

> "(...) E aí você vai me perguntar 'pra que que serve o Naldo?' Hoje, infelizmente - e eu falo infelizmente mesmo - o Naldo serve de chacota para podcasts, para programas de televisão, infelizmente, cara. Por que se as pessoas prestassem um pouco mais atenção nas músicas (esquece as letras, cara, que são realmente, terríveis), mas a base instrumental das músicas dele tem algumas exceções, tem algumas coisas interessantes, cara."